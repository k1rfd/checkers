package checkers;

public class Piece {

	Checkers.Color c;
	boolean kingship;
	int xLocation, yLocation;
	public Square square;
	public char returnChar;
	
	public boolean movedThisTurn;

	public Piece(Checkers.Color c, Square s, char v) {
		this.c = c;
		this.square = s;
		returnChar = v;
	}

	public Checkers.Color getColor() {
		return c;
	}

	public boolean getKingship() {
		return kingship;
	}

	//replace with a copy of the piece that is kinged initially
	
	public void setKingship() {
		if (returnChar == 'r') {
			returnChar = 'R';
		}
		if (returnChar == 'b') {
			returnChar = 'B';
		}
		kingship = true;
	}

	public int getX() {
		return xLocation;
	}

	public int getY() {
		return yLocation;
	}

	public boolean onBoard() {
		if (xLocation < 0 || yLocation < 0) {
			return false;
		} else {
			return true;
		}
	}

}
