package checkers;

import java.util.ArrayList;
import java.util.Iterator;

public class Player {

	Checkers.Color c;
	int directionY;
	public boolean lost;
	Game thisGame;
	
	Strategy thisStrat;

	public Player(Checkers.Color c, int dir, Game thisGame, int[] scores, Strategy strat) {
		this.c = c;
		directionY = dir;
		this.thisGame = thisGame;
		thisStrat = strat;
	}

	public Move findBestMove(ArrayList<Move> foundMoves) {

		ArrayList<Move> bestMoves = new ArrayList<Move>();

		Move bestMove = null;
		Iterator<Move> iter = foundMoves.iterator();
		while (iter.hasNext()) {
			Move thisMove = iter.next();
			if (bestMove == null) {
				bestMove = thisMove;
			} else {
				if (thisMove.score > bestMove.score) {
					bestMoves.clear();
					bestMoves.add(thisMove);
					bestMove = thisMove;
				} else {
					if (thisMove.score == bestMove.score) {
						bestMoves.add(thisMove);
					}
				}
			}
		}
		if (bestMoves.size() != 0) {

			int i = thisGame.getRand().nextInt(bestMoves.size());
			bestMove = bestMoves.get(i);
		}
		return bestMove;
	}
}
