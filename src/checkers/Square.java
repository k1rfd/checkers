package checkers;

public class Square {

	public int x, y;
	Piece piece;
	char returnChar;
	
	Square northEastSquare;
	Square northWestSquare;
	Square southWestSquare;
	Square southEastSquare;
	

	public Square(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setPiece(Piece p) {
		piece = p;
	}

	public Piece getPiece() {
		return piece;
	}

	public void setVisualReturn(char c) {
		returnChar = c;
	}

	public char getVisualReturn() {
		return returnChar;
	}

	public Square getNorthEast(){
		return northEastSquare;
	}
	
	public Square getNorthWest(){
		return northWestSquare;
	}
	
	public Square getSouthEast(){
		return southEastSquare;
	}
	
	public Square getSouthWest(){
		return southWestSquare;
	}
	
	@Override
	public String toString() {
		return "[" + Board.chars[y] + (x+1) + "]";
	}

}
