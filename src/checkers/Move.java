package checkers;

import java.util.ArrayList;

public class Move {

	public Piece piece;
	Square startLocation;
	ArrayList<Square> EndLocations = new ArrayList<Square>();
	ArrayList<Square> RemoveLocations = new ArrayList<Square>();
	Square jumpedSquare;
	int score;
	Game thisGame;
	boolean kinged;

	public Move(Square s, Square Jumped, Square end) {
		this.piece = s.piece;
		jumpedSquare = Jumped;
		startLocation = s;
		EndLocations.add(end);
	}

	public void setScore(int add) {
		score = add;
	}

	public void addScore(int add) {
		score += add;
	}
	public void subtractScore(int sub){
		score -= sub;
	}

	public void addEndLocation(Square s) {
		EndLocations.add(s);
	}

	public Square getLastEndLocation() {
		return EndLocations.get(EndLocations.size() - 1);
	}

	public void addRemoveLocation(Square s) {
		RemoveLocations.add(s);
	}

	@Override
	public String toString() {
		if (jumpedSquare != null) {
			return "(" + piece.getColor() + startLocation.toString() + "≥" + EndLocations.get(0).toString() + "{"
					+ score + "})";
		} else {
			return "(" + piece.getColor() + startLocation.toString() + ">" + EndLocations.get(0).toString() + "{"
					+ score + "})";
		}
	}

}
