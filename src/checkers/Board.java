package checkers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

public class Board implements Cloneable{
	
	Random rand = new Random(System.currentTimeMillis());

	Square[][] squares = new Square[8][8];
	static ArrayList<Piece> RedPiecesOffBoard = new ArrayList<Piece>();
	static ArrayList<Piece> BlackPiecesOffBoard = new ArrayList<Piece>();

	static int[] RedScores = new int[4];
	static int[] BlackScores = new int[4];
	
	Game thisGame;
	
	static char[] chars = new char[8];
	
	public Board(Game thisGame) {
		init();
		this.thisGame = thisGame;
	}
	public Board(Board otherBoard) {
		this.thisGame = otherBoard.thisGame;
		copyBoardPositions(otherBoard);
	}
	
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		Board clonedBoard = new Board(thisGame);
		return super.clone();
		
	}

	public void copyBoardPositions(Board oldBoard){
		
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (((x + y) % 2) == 0) {
					Square newSquare = new Square(x, y);
					squares[x][y] = newSquare;
					if(oldBoard.squares[x][y].getPiece() != null){
						newSquare.setPiece(oldBoard.squares[x][y].getPiece());
						newSquare.setVisualReturn(oldBoard.squares[x][y].getVisualReturn());
					}
					}
				}
			}
		}
	
	public void init() { // Create all pieces, place in starting locations,
							// clear off-board arraylists, set scores

		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (((x + y) % 2) == 0) {
					Square thisSquare = new Square(x, y);
					squares[x][y] = thisSquare;
					if (y < 3) {
						Piece thisPiece = new Piece(Checkers.Color.RED, thisSquare, 'r');
						thisSquare.setPiece(thisPiece);
					} else if (y > 4) {
						Piece thisPiece = new Piece(Checkers.Color.BLACK, thisSquare, 'b');
						thisSquare.setPiece(thisPiece);
					} else {
						// empty square
						thisSquare.setPiece(null);
					}
				}
			}
		}
		
		assignSquareLinks();

		RedPiecesOffBoard.clear();
		BlackPiecesOffBoard.clear();
		
		chars[0] = 'A';
		chars[1] = 'B';
		chars[2] = 'C';
		chars[3] = 'D';
		chars[4] = 'E';
		chars[5] = 'F';
		chars[6] = 'G';
		chars[7] = 'H';

		BlackScores = RedScores;

		/*
		 * SQUARES[1][3].piece = SQUARES[0][2].piece; SQUARES[0][2].piece = null; SQUARES[2][4].piece = SQUARES[3][5].piece; SQUARES[3][5].piece = null;
		 */

	}
	public void assignSquareLinks(){
		
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (((x + y) % 2) == 0) {
						if(newLocationInBounds(x+1, y-1)){
							squares[x][y].northEastSquare = squares[x+1][y-1];
						}
						if(newLocationInBounds(x-1, y-1)){
							squares[x][y].northWestSquare = squares[x-1][y-1];
						}
						if(newLocationInBounds(x+1, y+1)){
							squares[x][y].southEastSquare = squares[x+1][y+1];
						}
						if(newLocationInBounds(x-1, y+1)){
							squares[x][y].southWestSquare = squares[x-1][y+1];
						}
					}
				}
			}
		}
		
	/*
	 * for(int i = 0; i < 24; i++){ if(i%2 == 0){ Piece thisPiece = new Piece(Checkers.Color.RED, i*2); //order: color, positionHorizontal, positionVertical } else { Piece thisPiece = new
	 * Piece(Checkers.Color.BLACK, ); } } }
	 */
	public void makeMove(Move moveToExecute) {
		if(moveToExecute.jumpedSquare == null){
			moveToExecute.EndLocations.get(0).piece = moveToExecute.startLocation.piece;
			moveToExecute.startLocation.piece = null;
		}	else	{
			if(moveToExecute.piece.getColor() == Checkers.Color.RED){
				BlackPiecesOffBoard.add(moveToExecute.jumpedSquare.piece);
			}	else	{
				RedPiecesOffBoard.add(moveToExecute.jumpedSquare.piece);
			}
			moveToExecute.jumpedSquare.piece = null;
			moveToExecute.EndLocations.get(0).piece = moveToExecute.startLocation.piece;
			moveToExecute.startLocation.piece = null;
		}
	}

	static public int getRedPieces() {
		return 24 - RedPiecesOffBoard.size();
	}

	static public int getBlackPieces() {
		return 24 - BlackPiecesOffBoard.size();
	}

	public Square[][] getSquares() {
		return squares;
	}

	public Random getRand() {
		return rand;
	}

	public void printVisual() {
		// System.out.println(PieceLocations[0][0].getColor());
		System.out.println("   1  2  3  4  5  6  7  8");
		for (int y = 0; y < 8; y++) {
			System.out.print(chars[y] + "  ");
			for (int x = 0; x < 8; x++) {

				if (squares[x][y] != null) {
					if (squares[x][y].piece != null) {
						System.out.print(squares[x][y].piece.returnChar + "");
					} else {
						System.out.print("▢");
					}
				} else {
					System.out.print("▉");
				}

				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println(getBoardScore(squares));
	}
	public static boolean newLocationInBounds(int x, int y) {
		if (x >= 0 && x < 8 && y >= 0 && y < 8) {
			return true;
		} else {
			return false;
		}
	}
	
	public static float getBoardScore(Square[][] squares) {
		if(BlackPiecesOffBoard.size() == 12){
			return Integer.MAX_VALUE;
		}
		if(RedPiecesOffBoard.size() == 12){
			return Integer.MIN_VALUE;
		}
		int sum = 0;
		for (int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if (((x + y) % 2) == 0) {
					Square thisSquare = squares[x][y];
					if(thisSquare.getPiece() != null){
						if(thisSquare.getPiece().getColor() == Checkers.Color.RED){
							if(thisSquare.getPiece().getKingship()){
								sum+=2;
							}	else	{
								sum++;
							}
							sum-=(Math.abs(4 - thisSquare.x))/4;
						}	else	{
							if(thisSquare.getPiece().getKingship()){
								sum-=2;
							}	else	{
								sum--;
							}
							sum+=(Math.abs(4 - thisSquare.x))/4;
						}
					}
				}
			}
		}
		return sum;
	}
	//////
	public Node getBestMovePath(int depth, Checkers.Color c){
		
		//depth corresponds to equal depth in minimax algorithm when method is called
		if(depth == 0){
			return null;
		}
		for(int y = 0; y < 8; y++){
			for(int x = 0; x < 8; x++){
				//check that the given square contains a piece of the correct color
				if(squares[x][y] != null && squares[x][y].getPiece() != null && squares[x][y].getPiece().getColor() == c){
					//assign current square to placeholder thisSquare
					Square thisSquare = squares[x][y];
					//get return list for findMoves()
					ArrayList<Move> allMoves = findMoves(thisSquare, squares, c);
					//iterate through eligible moves for given square
					for(int i = 0; i < allMoves.size(); i++){
						//assign move to new Node
						Node thisNode = new Node(allMoves.get(i), getBoardScore(squares));
						//set child nodes for new Node (recursion), should result in between 0 - 4 possible moves depending on situation
						thisNode.add(getBestMovePath(depth-1, c.otherColor(c)));
					}
				}
			}
		}
		return null;
		
	}
	
	public ArrayList<Move> findMoves(Square thisSquare, Square[][] squares, Checkers.Color c){
		//create list to return
		ArrayList<Move> list1 = new ArrayList<Move>();
			//get all available single-square moves
			list1 = getSingleMove(thisSquare, squares, c);
			//append all available jump moves
			list1.addAll(getJumpMove(thisSquare, squares, c));
		return list1;
	}

	public ArrayList<Move> getSingleMove(Square thisSquare, Square[][] squares, Checkers.Color color) {
		//create list to return
		ArrayList<Move> possibleMoves = new ArrayList<Move>();
		//check that the Square contains a piece
		if (thisSquare.piece != null) {
			//check possible single moves for RED
			if (color == Checkers.Color.RED) {
				/* SouthWest square (X)
				 * ▉ R ▉
				 * X ▉ ▢
				 */
				//if South West square is on the board and empty (movable)
				if (thisSquare.getSouthWest() != null && thisSquare.getSouthWest().piece == null) {
					Move newMove = new Move(thisSquare, null, thisSquare.getSouthWest());
					possibleMoves.add(newMove);
				}
				// SE
				if (thisSquare.getSouthEast() != null && thisSquare.getSouthEast().piece == null) {
					Move newMove = new Move(thisSquare, null, thisSquare.getSouthEast());
					possibleMoves.add(newMove);

				}
				//check other two directions for a king
				if (thisSquare.piece.getKingship()) {
					// NW
					if (thisSquare.getNorthWest() != null && thisSquare.getNorthWest().piece == null) {
						Move newMove = new Move(thisSquare, null, thisSquare.getNorthWest());
						possibleMoves.add(newMove);

					}
					// NE
					if (thisSquare.getNorthEast() != null && thisSquare.getNorthEast().piece == null) {
						Move newMove = new Move(thisSquare, null, thisSquare.getNorthEast());
						possibleMoves.add(newMove);

					}
				}
			} else {
				// NW
				if (thisSquare.getNorthWest() != null && thisSquare.getNorthWest().piece == null) {
					Move newMove = new Move(thisSquare, null, thisSquare.getNorthWest());
					possibleMoves.add(newMove);

				}
				// NE
				if (thisSquare.getNorthEast() != null && thisSquare.getNorthEast().piece == null) {
					Move newMove = new Move(thisSquare, null, thisSquare.getNorthEast());
					possibleMoves.add(newMove);

				}
				if (thisSquare.piece.getKingship()) {
					// SW
					if (thisSquare.getSouthWest() != null && thisSquare.getSouthWest().piece == null) {
						Move newMove = new Move(thisSquare, null, thisSquare.getSouthWest());
						possibleMoves.add(newMove);

					}
					// NE
					if (thisSquare.getSouthEast() != null && thisSquare.getSouthEast().piece == null) {
						Move newMove = new Move(thisSquare, null, thisSquare.getSouthEast());
						possibleMoves.add(newMove);

					}
				}
			}
		}
		//return moves list
		return possibleMoves;
	}

	///////////////////////////////
	public ArrayList<Move> getJumpMove(Square thisSquare, Square[][] squares, Checkers.Color color) {
		ArrayList<Move> possibleMoves = new ArrayList<Move>();
		if(thisSquare.piece != null && thisSquare.piece.getColor() == color){
			if(color == Checkers.Color.RED){
				//SW
				// If there's a square to the southwest and it has a piece and it's the other color...
				if(thisSquare.getSouthWest() != null && thisSquare.getSouthWest().piece != null && thisSquare.getSouthWest().piece.getColor() != color){
					// if the next square to the southwest exists and it has no piece on it...
					if(thisSquare.getSouthWest().getSouthWest() != null && thisSquare.getSouthWest().getSouthWest().piece == null){
						// then we can make a jump move to it.
						Move newMove = new Move(thisSquare, thisSquare.getSouthWest(), thisSquare.getSouthWest().getSouthWest());
						possibleMoves.add(newMove);

					}
				}
				//SE
				if(thisSquare.getSouthEast() != null && thisSquare.getSouthEast().piece != null && thisSquare.getSouthEast().piece.getColor() != color){
					if(thisSquare.getSouthEast().getSouthEast() != null && thisSquare.getSouthEast().getSouthEast().piece == null){
						Move newMove = new Move(thisSquare, thisSquare.getSouthEast(), thisSquare.getSouthEast().getSouthEast());
						possibleMoves.add(newMove);

					}
				}
				if(thisSquare.piece.getKingship()){
					//NW
					if(thisSquare.getNorthEast() != null && thisSquare.getNorthEast().piece != null && thisSquare.getNorthEast().piece.getColor() != color){
						if(thisSquare.getNorthEast().getNorthEast() != null && thisSquare.getNorthEast().getNorthEast().piece == null){
							Move newMove = new Move(thisSquare, thisSquare.getNorthEast(), thisSquare.getNorthEast().getNorthEast());
							possibleMoves.add(newMove);

						}
					}
					//NE
					if(thisSquare.getNorthWest() != null && thisSquare.getNorthWest().piece != null && thisSquare.getNorthWest().piece.getColor() != color){
						if(thisSquare.getNorthWest().getNorthWest() != null && thisSquare.getNorthWest().getNorthWest().piece == null){
							Move newMove = new Move(thisSquare, thisSquare.getNorthWest(), thisSquare.getNorthWest().getNorthWest());
							possibleMoves.add(newMove);

						}
					}
				}
			}	else	{
					//NW
					if(thisSquare.getNorthWest() != null && thisSquare.getNorthWest().piece != null && thisSquare.getNorthWest().piece.getColor() != color){
						if(thisSquare.getNorthWest().getNorthWest() != null && thisSquare.getNorthWest().getNorthWest().piece == null){
							Move newMove = new Move(thisSquare, thisSquare.getNorthWest(), thisSquare.getNorthWest().getNorthWest());
							possibleMoves.add(newMove);

						}
					}
					//NE
					if(thisSquare.getNorthEast() != null && thisSquare.getNorthEast().piece != null && thisSquare.getNorthEast().piece.getColor() != color){
						if(thisSquare.getNorthEast().getNorthEast() != null && thisSquare.getNorthEast().getNorthEast().piece == null){
							Move newMove = new Move(thisSquare, thisSquare.getNorthEast(), thisSquare.getNorthEast().getNorthEast());
							possibleMoves.add(newMove);

						}
					}
					if(thisSquare.piece.getKingship()){
						//SW
						if(thisSquare.getSouthWest() != null && thisSquare.getSouthWest().piece != null && thisSquare.getSouthWest().piece.getColor() != color){
							if(thisSquare.getSouthWest().getSouthWest() != null && thisSquare.getSouthWest().getSouthWest().piece == null){
								Move newMove = new Move(thisSquare, thisSquare.getSouthWest(), thisSquare.getSouthWest().getSouthWest());
								possibleMoves.add(newMove);

							}
						}
						//SE
						if(thisSquare.getSouthEast() != null && thisSquare.getSouthEast().piece != null && thisSquare.getSouthEast().piece.getColor() != color){
							if(thisSquare.getSouthEast().getSouthEast() != null && thisSquare.getSouthEast().getSouthEast().piece == null){
								Move newMove = new Move(thisSquare, thisSquare.getSouthEast(), thisSquare.getSouthEast().getSouthEast());
								possibleMoves.add(newMove);

							}
						}
					}
			}
		}
//		for(int i = 0; i < possibleMoves.size(); i++){
//			if(possibleMoves.get(i).piece.movedThisTurn){
//				possibleMoves.remove(possibleMoves.get(0));
//			}	else	{
//				possibleMoves.get(i).piece.movedThisTurn = true;
//			}
//		}
		return possibleMoves;
	}
	///////////////////////////////
	
}
