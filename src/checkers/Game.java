package checkers;

import java.util.ArrayList;
import java.util.Random;

public class Game {

	GameState state = GameState.NOT_STARTED;
	int turns;
	boolean showConsole;
	boolean draw;
	int[] scores = new int[4];
	int[] moddedScores = new int[4];
	ArrayList<String> movesTaken = new ArrayList<String>();
	ArrayList<String> duplicateMoves = new ArrayList<String>();

	Random rand = new Random(System.currentTimeMillis());

	public void start() {

		Board thisBoard = new Board(this);
		//state = GameState.IN_PROGRESS;
		
		if(showConsole){
			thisBoard.printVisual();
		}
		System.out.println("Game start.");
		System.out.println();

		scores[0] = 5; // single move
		scores[1] = 10; // jump move
		scores[2] = 15; // jump king
		scores[3] = 10; // become king

		moddedScores[0] = 10;
		moddedScores[1] = 10; // all random
		moddedScores[2] = 10;
		moddedScores[3] = 10;

		Strategy Strat = new Strategy();
		
		// create players (red plays upwards, black plays downwards)
		Player playerOne = new Player(Checkers.Color.RED, 1, this, scores, Strat);
		Player playerTwo = new Player(Checkers.Color.BLACK, -1, this, moddedScores, Strat);
		
		//ArrayList<Move> moveTest = thisBoard.getSingleMove(thisBoard.squares[2][2], thisBoard.squares, 1, Checkers.Color.RED);
		
		String str = new String("GO");
		System.out.println(str.charAt(3));
		
		//ArrayList<Node> nodeList = thisBoard.getAllMoves(3, Checkers.Color.RED, thisBoard, Checkers.Color.RED);
		System.out.println(minimax(thisBoard, nodeList.get(0), 3, Integer.MIN_VALUE, Integer.MAX_VALUE, true));
		System.out.println(Checkers.Color.otherColor(Checkers.Color.RED));
		System.out.println(Checkers.Color.otherColor(Checkers.Color.BLACK));
		//playBestMove(thisBoard.getAllMoves(1,  Checkers.Color.RED));
		
		if(state == GameState.IN_PROGRESS){
			//play(playerOne, playerTwo, thisBoard);
		}
		// System.out.println(playerOne.findBestMove(playerOne.getAllMoves(thisBoard.getSquares())).toString());

	}

	public float minimax(Board thisBoard, Node rootNode, int depth, float alpha, float beta, boolean isMaxPlayer){
		float value = Integer.MIN_VALUE ;	
		
		if(depth == 0){
			return thisBoard.getBoardScore(thisBoard.squares);
		}
		
		if(isMaxPlayer){
			for(int i = 0; i < rootNode.replyNodes.size(); i++){
				value = Math.max(value, minimax(thisBoard, rootNode.replyNodes.get(i), depth-1, alpha, beta, false));
				alpha = Math.max(alpha, value);
				if(alpha >= beta){
					break;
				}
			}
			return value;
			
		}	else	{
			value = Integer.MAX_VALUE;
			for(int i = 0; i < rootNode.replyNodes.size(); i++){
				value = Math.min(value, minimax(thisBoard, rootNode.replyNodes.get(i), depth-1, alpha, beta, true));
				beta = Math.min(alpha, value);
				if(alpha >= beta){
					break;
				}
			}
			return value;
		}
			
	}

	enum GameState {

		NOT_STARTED, IN_PROGRESS, ENDED

	}

	public Game(boolean showConsole) {
		this.showConsole = showConsole;
	}

	public Random getRand() {
		return rand;
	}

}
