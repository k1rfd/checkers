package checkers;

import java.util.ArrayList;

public class Node {

	public Move myMove;
	public Node right;
	public Node left;
	public float value;
	public ArrayList<Node> replyNodes;
	
	public Node(Move thisMove, float score){
		myMove = thisMove;
		value = score;
	}
	public void add(Node n){
		if(n.value > value){
			if(right == null){
				right = n;
			}	else	{
				right.add(n);
			}
		}	else	{
			if(left == null){
				left = n;
			}	else	{
				left.add(n);
			}
		}
	}
	
}
