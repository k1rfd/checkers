package checkers;

import java.util.ArrayList;

public class Checkers {

	public static ArrayList<String> RESULTS = new ArrayList<String>();
	public static int redWins;
	public static int blackWins;
	public static int draws;
	public static int gamesAbandoned;

	public static void main(String[] args) {
		Checkers obj = new Checkers();
		obj.init();
	}

	public void init() {

		for (int i = 0; i < 1; i++) {
			Game thisGame = new Game(true);	//showOutput
			thisGame.start();
			
		}
		System.out.println("Results: ");
		System.out.println("     " + RESULTS);
		System.out.println("     Red: " + redWins);
		System.out.println("     Black: " + blackWins);
		System.out.println("     Abandoned: " + gamesAbandoned);
		if(draws > 0){
			System.out.println("     Draws:" + draws);
		}
	}

	public enum Color {

		RED, BLACK;
		
		static Color otherColor(Color c){
			if(c == Color.RED){
				return Color.BLACK;
			}	else	{
				return Color.RED;
			}
		}

	}

}
